const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'DataTrucker.IO',
  tagline: 'A simple no-code / less-code API Backend as a Service',
  url: 'https://www.datatrucker.io/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/branding-website_logo.svg',
  organizationName: 'DataTrucker.IO Inc, Ontario Canada', // Usually your GitHub org/user name.
  projectName: 'DataTrucker.IO', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'DataTrucker.IO',
      logo: {
        alt: 'DataTrucker.IO Logo',
        src: 'img/branding-website_logo.svg',
      },
      items: [
        {
          type: 'doc',
          docId: 'Intro',
          position: 'left',
          label: 'Documentation',
        },
        {to: '/blog', label: 'Blog', position: 'left'},
        {
          href: 'http://creativecommons.org/licenses/by-sa/4.0/',
          label: 'This work is licensed under CC BY-SA 4.0',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Downloads',
          items: [
            {
              label: 'Releases',
              href: 'https://github.com/gauravshankarcan/datatrucker.io/releases',
            },
            {
              label: 'Docker Registry',
              href: 'https://hub.docker.com/u/datatruckerio',
            },
          ],
        },
        {
          title: 'Connect',
          items: [
            {
              label: 'Issues / Bug / Feature Requests',
              href: 'https://github.com/gauravshankarcan/datatrucker.io/issues',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: '/blog',
            },
            {
              label: 'GitHub',
              href: 'https://github.com/gauravshankarcan/datatrucker.io',
            },
            {
              label: 'Tutorial',
              to: '/docs/Intro',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} DataTrucker.io, Inc, Ontario, Canada`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          //  editUrl:  'https://www.datatrucker.io/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          //editUrl: '/blog',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
