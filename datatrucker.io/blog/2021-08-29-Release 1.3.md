---
slug: Datatrucker-1.3
title: Datatrucker 1.3
author: Gaurav Shankar
tags: [Release]
---
<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->

## Release 1.3

#### Release of Operator and Kubernetes Capability
https://operatorhub.io/operator/datatrucker-operator

[Documentation](/docs/Overview/Going%20Operators)