---
slug: Datatrucker-1.0
title: Datatrucker 1.0
author: Shankar Janardhan
tags: [Release]
---
<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->

## Release 1.0

##### Launch of Datatrucker IO

A Simple API tool which allows API development with no/less code

https://www.datatrucker.io/


##### DB APIS – Oracle / Postgres / MYSQL / MSSQL / SQL-LIte

[knowledge-base/Database API](/docs/Database%20API/MariaDB)


##### Simple User management

[knowledge-base/Groups-management](/docs/Access/Group%20Management)

[knowledge-base/User-management](/docs/Access/User%20Management)

##### Introducing API Chaining

[knowledge-base/chains](/docs/General/Chaining%20APIs)


##### Production Hardening Guide

[knowledge-base/production-build-guide](/docs/Overview/Production%20Hardening)

##### Conversion of UI to react.js
