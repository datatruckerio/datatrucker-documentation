---
sidebar_position: 1
---
<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->

## Writing a Validation

APIs which support the “validations” tag uses standard AJV validation schemas and detailed design options can be [found here](https://github.com/ajv-validator/ajv/blob/master/docs/json-schema.md#type). Most of the resources provided by data trucker use the input of JSON format, this data needs to be sanitized for processing before the job handler picks up the data. AJV schemas servers as input sanitization and verifies the input schema before the actual workload.

AJV provides us a run kit to test various input schemas [available here](https://npm.runkit.com/ajv)

#### Here is a sample code block on writing ajv schemas for

    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "ajvtest",
            ...
            ...
            ...
            "validations": {
              "type": "object",
              "properties": {
                "field1": {
             ----> field1 represents data input when this resource is queried
                  "type": "string",
                  "pattern": "^[a-z0-9]*$",
                  "maxLength": 8
                }
              }
            }
          }

    Response: 201 OK
