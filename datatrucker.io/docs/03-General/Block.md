<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description
-----------

Create an API to perform conditional execution or iterative execution on other existing resources. It acts as a wrapper providing conditional/loopable execution of an existing resource.

The block API always returns an array.

The block API provides a when clause and loop clause,

- The when variable takes a boolean , true allows the resource to be executed , false returns an empty array . This makes it useful for conditional execution when used in chains
- The loop clause takes an Array as input, each element of the array should be a json and is added to the body of the request the block is executing, and returns the result set .so if the inbound size of tthe array is 5 the outbound response is also 5 

Notes: when and loop are keywords and cannot be used when encounter by a block for processing data

UI
--

Access the DataTrucker URL via a browser

### Make an API

### Create a Block Resource

1.  Go to Chain API in the Sidebar > Block 
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  Method: The type of REST Call
    3.  Linked resource: the resource/api in data trucker the block need to wrap around ( for conditiona and|or looping)
    4.  Linked resource method - GET/POST etc method to use to execute the resource being wrapped


### Query the resource you created

    #############################
    ### query a block endpoint
    #############################
    
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
      "when": true,
      "loop":[
          {
              "key": "value"
          },
          {
              "key": "value"
          }
      ]
    }
    
    Response: 200 OK
    {
       jsondata....
    }    


Just conditional execution


    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
      "when": true,
      "key": "value"
    }

    Response: 200 OK
    {
       jsondata....
    }      

As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
        resourcename: block
        type": Block
        restmethod": POST
        resourcelinkedmethod: POST
        resourcelink: proxy1
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest

      
API
---


### Create the API via REST

    #############################
    ## create a Proxy API create request 
    #############################
    
    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "block",
            "type": "Block",
            "restmethod": "POST",
            "resourcelinkedmethod": "POST",
            "resourcelink": "proxy1"
    }
    
    Response: 201 OK
    
    