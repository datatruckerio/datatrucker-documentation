<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->


Getting a login token (JWT)
---------------------------

All resources are wrapped by Authentication , before any API resource is executed it is necessary to get a JWT token . Below is a mechanism to JWT token

###### Conditions to be met:

*   A user ID is required.
*   An RBAC to the tenant where the API is deployed needs to be set up on the user.
*   If Keyclock is used: tenancy mapping of the Keycloak Roles need to be setup
*   User ID should have “Execute” access

Process to get obtain JWT is defined here : **[Login](/docs/General/Logging%20Token)**

Querying the API
----------------

    Sample request form :
    
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
    		"<input_key>": "<value>",
    		"<input_key>": "<value>",
    		"<input_key>": "<value>",
    		"<input_key>": "<value>"
    }
    
    Response: 200 OK
    {
       jsondata....
    }

The URL of the API would normally be ***hostname:port*** unless a pr