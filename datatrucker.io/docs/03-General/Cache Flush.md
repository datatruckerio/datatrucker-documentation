<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->


Description
-----------

The Cache Flush API removes local stored cache on the specific node, if you have cluster infrastructure then this API needs to be executed on every node.

Flush API allows removing the following caches

*   Resources – Memorized resource files
*   Credentials – Memorized Creds to avoid a round trip to DB
*   AJV – Validation schemas
*   Cred Handlers – Active connection objects to various systems

*   _Note:_
    *   _If you have multiple servers in the cluster, you need to execute this API against all servers_

API
---

### Fire against a single API server

    #############################
    ## Flush the Credential Cache
    #############################
    
    Header Token can be only Admin tenant, with write access.
    
    URL: /api/v1/cache/
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK
    {
        "reqCompleted": true,
        "date": "2021-01-22T14:10:03.040Z",
        "reqID": 5,
        "data": "cache flushed",
        "serverID": "ServerHandler"
    }

## In kuberentes / openshift environment

Its better to do a roll out to bounce the pod for creating cache , Kuber rollout strategy takes care of the clearing the pods