<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description
-----------

Create an API to allow to search through a given data, and pull out an approximate/probability string match .

UI
--

Access the DataTrucker URL via a browser

### Make an API

1.  Go to Utils API in the Sidebar > Oracle API’s
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  Method: The type of REST Call
    3.  Script: This details on the configuration options available for fuzzy search, the detailed available options are present below in Basic Options
    4.  Validation Regex of Input values, input sanitization before querying the database
        *   Examples provided in the UI when you try to create a new API

### Query the resource you created

    ## Note: send the data along with the API, in the event the data source is external, use the chain resource to fetch the data from an external source and pass it through
    
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
              "searchbase": [{
                "title": "Old Man's War",
                "author": {
                  "name": "John Scalzi",
                  "tags": [{
                    "value": "American"
                  }]
                }
              }, {
                "title": "The Lock Artist",
                "author": {
                  "name": "Steve Hamilton",
                  "tags": [{
                    "value": "English"
                  }]
                }
              }],
              "key": "englsh"
          }
    
    Response: 200 OK



As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
      - resourcename: fuze1
        name: fuze1
        type: Util-Fuzzy
        tenant: Admin
        restmethod: POST
        options:
          keys:
          - author.tags.value
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest

      
API
---

### Create the API via REST

    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "fuze1",
            "type": "Util-Fuzzy",
            "restmethod": "POST",
              "options": {
                "keys": ["author.tags.value"]
              }
    }
    
    Response: 201 OK
    

Options
-------

Note: we use an opensource library as a dependency and the configurations of options can be found at link [Fuse.js](https://fusejs.io/api/options.html)
