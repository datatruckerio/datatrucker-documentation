<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description
-----------

Create an API to allow to proxy request to an external rest end point using axios.

This service uses axios api and the full api documentation is available [here](https://www.npmjs.com/package/axios#axios-api)

Notes:

- The proxy API comes in handy when you have multiple microservices and you need to interact with another micro service
- The proxy API allows to dynamically inject external data into a chain flow if used inside chains
- Proxy API can simple act as authentication wraper / side car for other API and you gain to get datatrucker.io advantages like access logging, keycloack authentication etc.

UI
--

Access the DataTrucker URL via a browser

### Make an API

### Create a proxy API

1.  Go to IOT API in the Sidebar > Proxy APIs
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  Method: The type of REST Call
    5.  Validation Regex of Input values, input sanitization before querying
        1.  Examples provided in the UI when you try to create a new API


### Query the resource you created

    #############################
    ### query a proxy endpoint
    #############################
    
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
      method: 'post',
      url: 'https://api.agify.io/',
      data: {
        firstName: 'Fred',
        lastName: 'Flintstone'
      }
    }
    
    Response: 200 OK
    {
       jsondata....
    }    
    

As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
      - resourcename: proxy
        name: proxy
        type: IOT-Proxy
        restmethod: POST
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest

      
API
---


### Create the API via REST

    #############################
    ## create a Proxy API create request 
    #############################
    
    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "proxy1",
            "type": "IOT-Proxy",
            "restmethod": "POST"
    }
    
    Response: 201 OK
    
    
