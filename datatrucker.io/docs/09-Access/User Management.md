<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description
-----------

Data trucker provides an option to create local users. These users are maintained in the database connected to the API server

A local user is efficient in authentication management is mostly the preferred way to use for high frequency API login calls.

The user management panel is only open to the Admin tenant

UI
--

### Create a user

Login into the data trucker URL

*   Click on Users in the side panel > Manage Users
*   Scroll down to User management and fill out the form for creating users.
*   Note: After a user is, a user must belong to a group/tenant to perform a login. Follow the user mapping KB article

### Search for a User

Login into the data trucker URL

*   Click on Users in the side panel > Manage Users
*   To search use the inbuilt table search

### Update a User

*   Click on Users in the side panel > Manage Users
*   Search for a specific user and Search for a specific user in the user management panel and click on the Load button

### Delete a user

*   Click on Users in the side panel > Manage Users
*   Search for a specific user and Search for a specific user in the user management panel and click on the Delete button

### Add/delete a User to group

*   Click on Groups in the side panel > User Group mapping
*   Type in the username and group name in the form at the end of the page (applies only to local users) . Do not Map users to keycloak groups

API
---

### Create user via REST

    URL: /api/v1/users
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
      "username": "userTruckerWrite",
      "password": "Secret@123",
      "enabled": false,
      "asset": "secondasset"
    }
    
    Response: 201 OK

### Search for a user via REST

    URL: /api/v1/users?
    TYPE: GET
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK
    {
        "reqCompleted": true,
        "date": "2021-12-15T03:22:14.510Z",
        "reqID": 7,
        "data": [
           ....
        ],
        "serverID": "ServerHandler"
    }
    
    ------------------------
    
    URL: /api/v1/users?username=user   -> username parameter is similarity match 
    TYPE: GET
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK
    {
        "reqCompleted": true,
        "date": "2020-12-15T03:22:14.510Z",
        "reqID": 7,
        "data": [
           ...
        "serverID": "ServerHandler"
    }

### Update user via REST

    URL: /api/v1/users/<username>
    TYPE: PUT
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
      "password": "Secret@123",
      "emailid": "userTruckerDelete@gmail.com",
      "enabled": true,
      "asset": "first asset"
    }
    
    Response: 200 OK
    {
        "reqCompleted": true,
        "date": "2021-12-15T03:22:49.244Z",
        "reqID": 8,
        "data": "User Updated: userTruckerDelete",
        "serverID": "ServerHandler"
    }

### Delete user via REST

    URL: /api/v1/users/<username>
    TYPE: Delete
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK\

User Mappings
-------------

### User Listings

    URL: /api/v1/usergrouplisting/
    TYPE: GET
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK
    }

### Add user to a group

    URL: /api/v1/addusertogroup
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY:
    {
    	"username": "user2",
    	"groupname": "TenantTruckerReader"
    }
    
    Response: 201 OK
    {
        "reqCompleted": true,
        "date": "2021-12-15T03:25:25.790Z",
        "reqID": 38,
        "data": "User Mapping Added: userTruckerWrite into group WriteGroup",
        "serverID": "ServerHandler"
    }

### Delete user from an group

    URL: /api/v1/deleteuserfromgroup/:id  #id is fetched from userlistings api
    TYPE: Delete
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK