<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description
-----------

Data trucker provides an option to create local and keycloak groups. These groups are maintained in the database connected to the API server

Groups are linked to tenants, if no groups exists then the tanant becomes inaccessible

The group management panel is only open to the Admin tenant

UI
--

### Create a Group

Login into the data trucker URL

*   Click on Groups in the side panel > Manage Groups
*   Scroll down to group management and fill out the form for creating groups.
*   Level:
    1.  Tenant Author access in the Admin tenant gives access to user management and group management features.
    2.  Tenant Author access in all tenants allows creating resources in their respective tenants.
    3.  Tenant Reader can only execute resources created by an author.
*   Tenant: Tenant of a group.
    *   If no groups exist for a given tenant, the tenant becomes inaccessible.
    *   If the tenant does not match any existing tenants, then it will create a new tenant
    *   If a group is created with a tenant which doesn’t match any existing tenant, a new tenant is automatically created
*   Type: if this is a Keycloak based group, then add the type “keycloak”
    *   Do not create users for keycloak, they automatically get synced from keycloak

### Search for a Group

Login into the data trucker URL

*   Click on Groups in the side panel > Manage Groups
*   To search for a group, type the group name utilizing the inbuilt table search

### Update a Group

*   Click on Groups in the side panel > Manage Groups
*   Search for a specific group in the group management panel and click on the Load button

### Delete a Group

*   Click on Groups in the side panel > Manage Groups
*   Search for a specific group in the group management panel and click on the delete button

API
---

### Create Groups via REST

    URL: /api/v1/groups
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
                  "groupname": "ReadGroup",
                  "level": "Tenant_Reader",
                  "tenantname": "Trucker",
                  "enabled": true,
                  "type":"local",
                  "asset": "Group Read Asset"
    }
    
    Response: 201 OK

### Search for a Groups via REST

    URL: /api/v1/groups?
    TYPE: GET
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK
    {
        "reqCompleted": true,
        "date": "2020-12-15T02:39:17.584Z",
        "reqID": 7,
        "data": [
           ...
        ],
        "serverID": "ServerHandler"
    }
    
    ------------------------
    
    URL: /api/v1/groups?groupname=Ten   -> groupname parameter is similarity match 
    TYPE: GET
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK
    {
        "reqCompleted": true,
        "date": "2021-12-15T02:39:17.584Z",
        "reqID": 7,
        "data": [
            ...
        ],
        "serverID": "ServerHandler"
    }

### Update Groups via REST

    URL: /api/v1/groups/<groupname>
    TYPE: PUT
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
        {
            "level": "Tenant_Author",
            "tenantname": "DeleteGroup",
            "enabled": true,
            "asset": "Group DeleteGroupAsset2",
            "type": "local"
        }
    
    Response: 200 OK

### Delete Groups via REST

    URL: /api/v1/groups/<groupname>
    TYPE: Delete
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK

### Get List of Tenants

    URL: /api/v1/tenants
    TYPE: GET
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    
    Response: 200 OK
    {
        "reqCompleted": true,
        "date": "2021-12-15T02:38:39.081Z",
        "reqID": 6,
        "data": [
           ...
        ],
        "serverID": "ServerHandler"
    }