<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description of Javascript API
-----------------------------

JS-API is an advanced API aimed at building a full Javascript based API, without the overhead on developers to build RBAC / authentication or a dedicated server to host APIs.

JS-API allows a simple promise based return to build Asynchronour Custom API using any node package.

UI API Editor
-------------

### Access the DataTrucker URL via a browser

### Create a JS-API

1.  Go to Script API in the Sidebar > JS APIs
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  Method: The type of REST Call
    3.  Script: The javascript filename
    4.  Validation Regex of Input values, input sanitization before querying
        1.  Examples provided in the UI when you try to create a new API \[ [Validation reference](/docs/General/Input%20Santization) \]

### Query the resource you created ( Javascript API )

    #############################
    ### query a fetch endpoint
    #############################
    
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
    	"<key>": "<value>",
            "<key>": "<value>",
            "<key>": "<value>"
    }
    
    Note: if this is a Get request you have to pass parameters as query keyvalues
    Example:
    URL: /api/v1/jobs/<resource name>?key1=value1&key2=value2
    
    Response: 200 OK
    {
       jsondata....
    }
    

![](../../static/diagrams/JSAPI/jsapi.png)


As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
      - resourcename: jsscript1
        name: jsscript1
        tenant: Admin
        type: Script-JS
        restmethod: PUT
        script: testscript.js
        validations:
          type: object
          properties:
            number:
              type: number
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest

API via REST
------------

### Create the API via REST ( Javascript API )

    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
        "resourcename": "jsscript1",
        "type": "Script-JS",
        "restmethod": "PUT",
        "script": "testscript.js",
            "validations":{
                "type": "object",
                "properties": {
                    "number": {
                        "type": "number"
                    }
                }
            }
    }
    
    Response: 201 OK

### Javascript API Module information

A sample script is provide below which multiplies 2 numbers. 

    // A Simple Javascript file will look like 
    // testscript.js
    
    // jobstats
    const runHandle = async (data) =>
          new Promise((resolve) => {
                data.number=parseInt(data.number)
                resolve(data.number * data.number)
          })
    
    exports.runHandle = runHandle

#### Conditions to be met

The end of the file should have “exports.runHandle = runHandle” this allows the job API read data back into its system

The data is a JSON object as passed from the request body or request query. It will have the same format( key/values nesting as the request body)

Each Key/value at the root(data JSON input object) level is validated against the regex, and nested values will not be tested.

To require additional libraries in the JS file. Packages can install them into package.json using npm install ***packagename***. Once installed, it can “required” inside the JS file.

If you need additional packages, they need to be installed again with each upgrade

Script files are searched in the folder as defined by config/resource.config.json -> key: “Templates.Scripts”.
