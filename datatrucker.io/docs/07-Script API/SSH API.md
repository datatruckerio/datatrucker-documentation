<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Create a command over SSH API
=============================

Description
-----------

The command over SSH API allows executing ssh commands over REST. This command is quite useful in triggering pre-defined commands in secured environments. From a security perspective, this allows external triggers(pre-defined commands) without providing Linux/CLI access to a specific user.

UI
--

Access the DataTrucker URL via a browser

### Create a credentials

1.  Go to Credentials in the sidebar > SSH Credentials
2.  Scroll down to Credential Management
3.  Fill the form to create the connection
    1.  Credential name – an arbitrary value to identify the credential object
    2.  The hostname of the target server to SSH. Note: SSH executes from the API server
    3.  SSH Username
    4.  SSH password or location of the ssh private key on the API server
    5.  Password is Privatekey, is to identify if step 4, is a password or file location
    6.  SSH Port
4.  Click on Create

### Create a ssh API

1.  Go to Script API in the Sidebar > SSHShell APIs
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  The Credential linked to the resources
    3.  Method: The type of REST Call
    4.  Script: Command to executes
    5.  Validation Regex of Input values is the input sanitization before executing the command. Arguments will be sent to API as an Array and will be space-separated. Every input argument needs to pass the regex as defined during the creation of the API.
    6.  [Documentation for Validation](/docs/General/Input%20Santization)

### Query the resource you created

    #############################
    ### query a ssh endpoint
    #############################
    
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
    		"args": ["-la"]
    }
    
    Note: if this is a Get request you have to pass parameters as query keyvalues
    Example:
    URL: /api/v1/jobs/<resource name>?args=<arg1>,<arg2>,....
    
    Response: 200 OK
    {
       jsondata....
    }
    

As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
      - resourcename: sshell1
        name: sshell1
        type: Script-SSH
        credentialname: sshlocalconnect
        tenant: Admin
        restmethod: GET
        script: echo $PATH
        validations:
          type:
          - object
          properties:
            args:
              type: string
              pattern: "^[-a-z0-9]*$"
              maxLength: 8
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest

      
API
---

### Create a credential via REST

    #############################
    ## create a SSH cerdential
    #############################
    
    URL: /api/v1/credentials/
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "credentialname": "sshlocalconnect",
            "type": "Script-SSH",
            "hostname": "localhost",
            "username": "root",
            "password": "root",
            "port": 22
    }
    
    Response: 201 OK
    
    ################################
    ## create a SSH Private cerdential
    ################################
    
    URL: /api/v1/credentials/
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "credentialname": "sshlocalconnectprivate",
            "type": "Script-SSH",
            "hostname": "localhost",
            "username": "app-admin",
            "password": "sshprivate.key",
            "port": 22,
            "passwordisPrivateKey": true
    }
    
    Note:  the folder location for keys can be configured in resource.config.json > Template.keys
    
    Response: 201 OK

### Create the API via REST

    #############################
    ## create a command over SSH API
    #############################
    
    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "sshell1",
            "type": "Script-SSH",
            "credentialname": "sshlocalconnect",
            "restmethod": "GET",
            "script": "echo $PATH",
            "validations": {
              "type": ["object"],
              "properties": {
                "args": {
                    "type": "string",
                    "pattern": "^[-a-z0-9]*$",
                    "maxLength": 8
                }
              }
            }
    }
    
    Response: 201 OK
