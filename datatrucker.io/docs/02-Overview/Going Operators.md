---
sidebar_position: 3
---
<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->


Installation
---------

The Screenshot here are from openshift similar process in kuberenetes

1. Log into the Operator hub and searchfor "datatrucker"

![alt](../../static/diagrams/operator/operartorhub.png)

2. Install the operator

![alt](../../static/diagrams/operator/operatorinstall.png)

3. open the installed operators to ensure installation

![alt](../../static/diagrams/operator/operartorlanding.png)

4. we have some ready made samples [here](https://gitlab.com/datatruckerio/datatrucker-server/-/tree/develop/samples/operator_samples) , change the namespace as you wish

5. There are 2 types of objects
   1. DataTrucker Configs
      1. Defines the server settings  look [here to configure it](/docs/Overview/Production%20Hardening)      
      2. start from the samples and then customize to your suitability 
   2. DataTrucker Flows
      1. Defines a job or an API endpoint


6. Watch the demo on our intro page to see how DataTruckerConfigs and DataTruckerFlows work together .


