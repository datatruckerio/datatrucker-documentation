---
sidebar_position: 2
---
<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->

## Preparation
```language

    Note:  The guide assumes you have already completed the getting started guide the database will be rebuilt completely during the production build ,
           this would means, you will have a brand new instance with no data . 

```

1.  You will need 1 private-public key pair for JWT ( this is used for signing authentication tokens issued fot logging in)
    1.  Sample key pair generations: [Here](https://www.ssh.com/academy/ssh/keygen#ssh-keys-and-public-key-authentication)
2.  A Randomly generated key and IV for encrypting credentials. Random generators can be found [here](https://www.allkeysgenerator.com/Random/Security-Encryption-Key-Generator.aspx)
3.  [Optional]: Key cloak instance if you plan on integrating external auths
4.  [Optional] SSL certs and CA.
    1.  Sample SSL Cert generation [here](https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/)

## Prerequisites

### Understanding a model of production-grade architecture

Note: 
1. Datatrucker only does the backend workload, 
2. standard security best practices will still apply and need to be verified on every server
3. All API server still needs to be fronted with nginx / api gateways / WAF.

The UI part of the datatrucker is only a development support tool (like an IDE). we follow API first policy . i.e. every functionality can be done with directly interacting with API server using REST. Hence in most cases it is not necessary to install UI server in production

Here is a sample architecture design

![](../../static/diagrams/connections.png)

## Configuring the API Server

### Server Configurations

cd into the datatrucker_api/config folder and edit the file **server.config.json**

#### Modifying the default ports and bind

    Bind: Default 0.0.0.0 ---> default network interface to listen to
    port: Default 8080 ----> port of the Datatrucker.io API server. recommend using a non-privileged port 

#### Modifying the protocol

    protocal.type : default http ---->  make it to https for tls based connection
    protocal.http2: default false ----> default use HTTP 1.1 ,, to use HTTP enable this to true [ Optional ]
    protocal.httpsconfig.allowHTTP1: default true ----> allow HTTP1 failback if HTTP2 isnt available
    protocal.httpsconfig.cert: default truckerapi.crt ----> SSL certs placed in the folder datatrucker_api/config/
    protocal.httpsconfig.key: default truckerapi.key ----> SSL certs placed in the folder datatrucker_api/config/

#### Keycloak Configurations

`Configuring Guide:`  [ here ](/docs/Access/Keycloak) 

#### General Configurations

    jobResponseErrors: default true ------> returns the error response back to the user.   ( for job API)
    requestLog: default false --------> prints user sent data into the logs
    compression.threshold: default 2048  ------------> response is compressed
    helmet: default framguard.action is deny -----> security headers from library https://www.npmjs.com/package/helmet 
    serverID: unique server name  ( helpful for tracing out in clustered operations )

#### Disable unnecessary components

    pluginsEnable: Default true ---> if a specific component is not used, we can disable the javascript from loading 

### Crypto Configurations

#### Password Hashing

Used to hash passwords for users created on the backend DB ( local non-keycloak users ) .

Note: if the users are created , and algorithm is changed users created before the change may became invalid(including admin user) as hash wont match. contact support for migration guidance.

    passwordHash.Algorithm : Default sha256 ---> execute "openssl list -digest-algorithms"  on linux  to get list of supported algorithms
    passwordHash.SaltLength : Default 16

#### Credentials encryptions

Unlike passwords, credentials need to be encrypted/decrypted when connecting to external systems. example ssh credentials / oracle credentials .

Note: for changes to this below credentials post initial setup, contact support for migration guidance.

    CredentailEncryption.IV : Default unsecured string--->  change IV  and key to your unique key ,  this should be same across the cluster
    CredentailEncryption.Key: Default unsecured string--->  change IV  and key to your unique key,  this should be same across the cluster
    CredentailEncryption.Algorithm: Default sha256 ---> execute "openssl list -cipher-algorithms" on linux  to get list of supported algorithms

#### JWT Signing keys

    JWT.signOptions.expiresIn: Default 60m--->   set less than 10 minutes in production environment
    JWT.signOptions.algorithm: Default RS256---> supported https://www.npmjs.com/package/jsonwebtoken#algorithms-supported
    JWT.authorization_keys.public_key: Default <file> in config directory ---> generate a new privatekey pair 
    JWT.authorization_keys.private_key:  Default <file> in config directory ---> generate a new privatekey pair 

### Resource Configurations

#### Cache config

    Cache.Resource ->  to enable caching Resources
    Cache.Creds->  to enable caching Creds
    Cache.ResourceFlush->  time in milliseconds to invalidate cache and do a round trip to file system to get new config file copy
    Cache.CredsFlush->  time in milliseconds to invalidate cache and do a round trip to DB system to get new copy of the credential

### Test your configurations

Once the above configurations are done test your server by starting it using the command “npm run start”
